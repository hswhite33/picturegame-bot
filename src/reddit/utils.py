'''Miscellaneous utility functions to be used with the retry decorator'''

from ..const import COMMENT_FOOTER
from ..utils.Retry import retry
from ..models import Logger

from praw.exceptions import APIException

@retry
def addContributor(subreddit, redditor):
    subreddit.contributor.add(redditor)


@retry
def removeContributor(subreddit, redditor):
    subreddit.contributor.remove(redditor)


@retry
def sendMessage(redditor, subject, message, subredditName, **kwargs):
    content = (message + COMMENT_FOOTER).format(subredditName = subredditName, **kwargs)
    try:
        redditor.message(subject, content)
    except APIException as e:
        # TODO #18 - can probably handle this more tidily with PRAW V7
        if e.error_type == "NOT_WHITELISTED_BY_USER_MESSAGE" or e.error_type == "PM_MODERATOR_RESTRICTION":
            # User doesn't let us DM them; give up
            Logger.info("Forbidden from sending DM to user", { "username": redditor.name })
            return
        raise e



@retry
def distinguishComment(comment, sticky = False):
    comment.mod.distinguish(sticky = sticky)


@retry
def commentReply(submissionOrComment, body, subredditName, sticky = False, **kwargs):
    replyComment = reply(submissionOrComment, body, subredditName, **kwargs)
    distinguishComment(replyComment, sticky)
    return replyComment


@retry
def reply(repliable, reply, subredditName, **kwargs):
    content = (reply + COMMENT_FOOTER).format(subredditName = subredditName, **kwargs)
    return repliable.reply(content)


@retry
def getPostAuthorName(submission):
    return submission.author.name


@retry
def deleteComment(comment):
    comment.delete()


@retry
def removeEntity(entity):
    entity.mod.remove()


@retry
def selectFlair(submission, flair):
    submission.flair.select(flair)


@retry
def getCreationTime(submissionOrComment):
    return submissionOrComment.created_utc

@retry
def getMods(subreddit):
    return set(map(lambda mod: mod.name, subreddit.moderator()))

@retry
def isDeletedOrRemoved(entity):
    return entity.author is None or entity.banned_by is not None

def decodeBase36Id(idString):
    alphabet = '0123456789abcdefghijklmnopqrstuvwxyz'

    res = 0

    for i, c in enumerate(reversed(idString)):
        res += alphabet.index(c) * (36 ** i)

    return res
