def escapeChars(name):
    '''Escape markdown characters to avoid usernames getting rendered weirdly in some cases.
    The only known case is with underscores, and that is only in a special case, but we'll do a generalized fix here.
    '''

    return name.replace('_', '\\_')
